//
// Created by Issam on 22/11/2017.
//

#ifndef PROJETDSA_INPUTSTREAM2_H
#define PROJETDSA_INPUTSTREAM2_H
#include <string>
#include <iostream>


class InputStream2 {
private:
    FILE * pFile;
    bool end_of_file;
    int fileSize;
    int filePos;

public:
    InputStream2();
    void open(std::string filename);
	uint32_t read_next();
    bool end_of_stream();
	void close();
};


#endif //PROJETDSA_INPUTSTREAM2_H
