#pragma once
#include <cstdint>
class OutputStreamBuff
{
	typedef struct
	{
		unsigned size;
		int pos;
		int* data;
	} Data_buffer;

private:
	int fileDescriptor;
	void * buf;
	Data_buffer data_buffer;
public:
	OutputStreamBuff(unsigned bufferSize);
	~OutputStreamBuff();
	void create(const char * filename);
	void write(uint32_t n);
	void close();
	void emptyBuffer();
};

