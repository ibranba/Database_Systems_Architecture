#include "stdafx.h"
#include "ExternalMultiWaySort.h"
#include <iostream>
#include <algorithm>

ExternalMultiWaySort::ExternalMultiWaySort(int m, int d, std::string filename, int fileSize) : filename(filename), fileSize(fileSize), nbMergeStep(d), memorySize(m)
{
	nbInputStream = fileSize / m;
	for (size_t i = 0; i < nbInputStream; i++)
	{
		nbEltToRead.push_back(m);
		inputs.push_back(new InputStreamMemMap());
		heapMerge.push_back(std::vector<int>());
	}
	//si 10 elt et m = 3, il faut 4 IS, le dernier ne lis que 1
	if (fileSize % m > 0) {
		nbInputStream++;
		nbEltToRead.push_back(fileSize % m);
		inputs.push_back(new InputStreamMemMap());
		heapMerge.push_back(std::vector<int>());
	}    

	out = new OutputStreamMemMap();
	outSorted = new OutputStreamMemMap();

	out->create(L"output.txt", 2621440);
	outSorted->create(L"outputSorted.txt", 2621440);
}

ExternalMultiWaySort::~ExternalMultiWaySort()
{
}

//Pour chaque partie : 
//  1.la trier
//  2.La reecrire en memoire

//Pour chaque d stream :
//  1.Merge comme multiway dans heap
//  2.Ajouter a la fin de la liste
//  3.Merge les d suivant
//  4.Jusqu'a ce qu'il en reste 1
//  5.L'ecrire dans un fichier

void ExternalMultiWaySort::start() {
	sortInputStreams();  //Trier tout les inputs streams et ecrire

	for (size_t i = 0; i < nbInputStream; i++)
	{
		inputs[i]->close();
		openFile(inputs[i], "outputSorted.txt", i, i * memorySize, true);
	}

	mergeBlocks();
	printHeap();
	writeOutput();
}

/*Merge all blocks, d by d*/
void ExternalMultiWaySort::mergeBlocks() {
	int mergedStream = 0;
	int min;
	std::vector<int> mergedDStream;
	while (mergedStream < heapMerge.size() - 1)
	{
		while (true)
		{
			//Merge les strms de mergedStream a mergedStream+d dans mergedDStream
			//Calcul min
			// ! la derniere pourrais en avoir moins que d! => faire min
			min = getMinValue(mergedStream, std::min((int)heapMerge.size(), mergedStream + nbMergeStep));
			if (min == std::numeric_limits<int>::max())
				break;
			mergedDStream.push_back(min);
		}
		heapMerge.push_back(mergedDStream); //Ajout de la nouvelle liste 
		posOnMergedHeap.push_back(0); //Position dans la nouvelle liste
		nbEltToRead.push_back(mergedDStream.size()); //nb d'element a lire dans la nouvelle liste

		mergedDStream.clear();
		mergedStream += nbMergeStep;
	}
}

/*
   Return std::numeric_limits<int>::max() si tout les streams sont eof
*/
int ExternalMultiWaySort::getMinValue(int begin, int end)
{
	int min = std::numeric_limits<int>::max();
	int choosedStream = -1;
	int pos;

	for (size_t i = begin; i < end; i++)
	{
		//On va sur le nb 0 si on est dans les streams, car se sont des piles
		// posOnMergedHeap[i - nbInputStream] si on est dans les listes merger, car il faut suivre l'elt courant apres les streams
		pos = (i < nbInputStream) ? 0 : posOnMergedHeap[i - nbInputStream];
		if (nbEltToRead[i] >= 0 && pos < heapMerge[i].size() && heapMerge[i][pos] < min)
		{
			min = heapMerge[i][pos];
			choosedStream = i;
		}
	}
	if (choosedStream == -1)
		return min;

	//min(choosedStream, nbInputStream - 1) : Plus de input stream si on atteind les listes deja sort
	readInteger(inputs[std::min(choosedStream, nbInputStream - 1)], choosedStream);
	return min;
}

void ExternalMultiWaySort::readInteger(InputStreamMemMap * inputstream, int position)
{
	nbEltToRead[position] --;
	if (position < nbInputStream) { //Si on a encore affaire au stream
		if (nbEltToRead[position] >= 0)
			heapMerge[position].insert(heapMerge[position].begin(), inputstream->read_next());
	} else { //Sinon, on est dans les listes deja sort
		posOnMergedHeap[position - nbInputStream]++;
	}
}

void ExternalMultiWaySort::openFile(InputStreamMemMap * inputstream, std::string filename, int position, int offset, bool read)
{
	std::wstring stemp = std::wstring(filename.begin(), filename.end());
	LPCWSTR sw = stemp.c_str();
	inputstream->openWithOffset(sw, 2621440, offset);
	if (read) readInteger(inputstream, position);
}

void ExternalMultiWaySort::printHeap()
{
	for (size_t i = 0; i < heapMerge.size(); i++)
	{
		for (size_t j = 0; j < heapMerge[i].size(); j++)
			std::cout << heapMerge[i][j] << " ";
		std::cout << std::endl;
	}
}

void ExternalMultiWaySort::writeOutput()
{
	for (size_t i = 0; i < nbInputStream; i++) {
		inputs[i]->close();
		delete(inputs[i]);
	}

	for (size_t i = 0; i < fileSize; i++)
		out->write(heapMerge[heapMerge.size()-1][i]);

	out->close();
	delete out;
}

void ExternalMultiWaySort::sortInputStreams() {
	std::vector<int> heap;
	for (size_t i = 0; i < nbInputStream; i++)
		openFile(inputs[i], filename, i, i * memorySize, false);

	for (size_t i = 0; i < nbInputStream; i++)
	{
		for (size_t j = 0; j < nbEltToRead[i]; j++)
			heap.push_back(inputs[i]->read_next());
		std::sort(heap.begin(), heap.end());
		for (size_t j = 0; j < nbEltToRead[i]; j++)
			outSorted->write(heap[j]);
		heap.clear();
	}
	outSorted->close();
	delete(outSorted);
}
