//
// Created by Issam on 22/11/2017.
//

#ifndef PROJETDSA_OUTPUSREAM2_H
#define PROJETDSA_OUTPUSREAM2_H
#include <string>

class OutputStream2 {
private:
    FILE * pFile;
public:
    OutputStream2();
    void create(std::string filename);
    void write(uint32_t & n);
    void close();
};


#endif //PROJETDSA_OUTPUSREAM2_H
