#include "stdafx.h"
#include "stdafx.h"
#include "InputStreamMemMap.h"
#include <iostream>
#include <string>
#include <exception>
#include <fstream>
#include <windows.h>
#include <stdio.h>
#include <strsafe.h>


InputStreamMemMap::InputStreamMemMap():readCounter{0},eof{false}, mappingopen{false}
{
}


InputStreamMemMap::~InputStreamMemMap()
{
	
}
void InputStreamMemMap::open(LPCWSTR aFilename, int numberOfElements)
{
	offset = 0;
	this->filename = aFilename;
	mappingSize = numberOfElements;
	nbRemElementsBuffer = numberOfElements;
	openFile(aFilename);
	createMapping(0, numberOfElements * sizeof(uint32_t));
	filesize = GetFileSizeEx(hFile, NULL);
}

void InputStreamMemMap::openWithOffset(LPCWSTR aFilename, int numberOfElements, int offset)
{
	this->offset = offset * sizeof(uint32_t);
	this->filename = aFilename;
	mappingSize = numberOfElements;
	nbRemElementsBuffer = numberOfElements;
	openFile(aFilename);
	createMapping(offset * sizeof(uint32_t), numberOfElements * sizeof(uint32_t));
	filesize = GetFileSize(hFile, NULL);
	filesize -= this->offset;
}

uint32_t InputStreamMemMap::read_next()
{	
	if (!eof) {
		uint32_t ret = *(data++);
		filesize -= sizeof(uint32_t);
		nbRemElementsBuffer--;
		if (filesize == 0)
			eof = true;
		if (nbRemElementsBuffer == 0 && !eof) {
			readCounter++;
			createMapping(offset + readCounter * (mappingSize * sizeof(uint32_t)), mappingSize * sizeof(uint32_t));
			nbRemElementsBuffer = mappingSize;
		}
		return ret;
	}
	else
		return -1;
}

bool InputStreamMemMap::end_of_stream()
{
	return eof;
}

void InputStreamMemMap::close()
{
	if (mappingopen) {
		BOOL bFlag;
		bFlag = UnmapViewOfFile(lpMapAddress);
		bFlag = CloseHandle(hMapFile); // close the file mapping object

		if (!bFlag)
		{
			_tprintf(TEXT("\nError %ld occurred closing the mapping object!"),
				GetLastError());
		}

		bFlag = CloseHandle(hFile);   // close the file itself

		if (!bFlag)
		{
			_tprintf(TEXT("\nError %ld occurred closing the file!"),
				GetLastError());
		}
		mappingopen = false;
	}
	
}

void InputStreamMemMap::openFile(LPCWSTR filename) {

	hFile = CreateFile(filename,
		GENERIC_READ | GENERIC_WRITE ,
		FILE_SHARE_READ | FILE_SHARE_WRITE ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		_tprintf(TEXT("hFile is NULL\n"));
		_tprintf(TEXT("Target file is %s\n"),
			filename);
		throw std::exception("Could not create file : INVALID_HANDLE_VALUE");
	}
}

void InputStreamMemMap::createMapping(DWORD FILE_MAP_START, DWORD BUFFSIZE) {

	// Close the file mapping object and the open file

	if (mappingopen) {
		BOOL bFlag = UnmapViewOfFile(lpMapAddress);

		bFlag = CloseHandle(hMapFile); // close the file mapping object
	}

	DWORD dwFileMapSize;  // size of the file mapping
	DWORD dwMapViewSize;  // the size of the view
	DWORD dwFileMapStart; // where to start the file map view
	DWORD dwSysGran;      // system allocation granularity
	SYSTEM_INFO SysInfo;  // system information; used to get granularity

	char * pData;         // pointer to the data
	int i;                // loop counter
	int iData;            // on success contains the first int of data
	int iViewDelta;       // the offset into the view where the data
						  //shows up

	
	// Get the system allocation granularity.
	GetSystemInfo(&SysInfo);
	dwSysGran = SysInfo.dwAllocationGranularity;

	dwFileMapStart = (FILE_MAP_START / dwSysGran) * dwSysGran;
	// Calculate the size of the file mapping view.
	dwMapViewSize = (FILE_MAP_START % dwSysGran) + BUFFSIZE;

	// How large will the file mapping object be?
	dwFileMapSize = FILE_MAP_START + BUFFSIZE;
	// The data of interest isn't at the beginning of the
	// view, so determine how far into the view to set the pointer.
	iViewDelta = FILE_MAP_START - dwFileMapStart;


	// Create a file mapping object for the file
	// Note that it is a good idea to ensure the file size is not zero
	hMapFile = CreateFileMapping(hFile,          // current file handle
		NULL,           // default security
		PAGE_READWRITE, // read/write permission
		0,              // size of mapping object, high
		dwFileMapSize,  // size of mapping object, low
		NULL);          // name of mapping object

	if (hMapFile == NULL)
	{
		_tprintf(TEXT("hMapFile is NULL: last error: %d\n"), GetLastError());
		throw std::exception("Could not create mapping");
	}

	// Map the view and test the results.

	lpMapAddress = MapViewOfFile(hMapFile,            // handle to
													  // mapping object
		FILE_MAP_ALL_ACCESS, // read/write
		0,                   // high-order 32
							 // bits of file
							 // offset
		dwFileMapStart,      // low-order 32
							 // bits of file
							 // offset
		dwMapViewSize);      // number of bytes
							 // to map
	if (lpMapAddress == NULL)
	{
		_tprintf(TEXT("lpMapAddress is NULL: last error: %d\n"), GetLastError());
		throw std::exception("Could not create mapping");
	}

	// Calculate the pointer to the data.
	pData = (char *)lpMapAddress + iViewDelta;

	// Extract the data, an int. Cast the pointer pData from a "pointer
	// to char" to a "pointer to int" to get the whole thing

	this->data = (uint32_t *)pData;
	mappingopen = true;
}
