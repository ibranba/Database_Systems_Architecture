#pragma once
#include <vector>
#include "InputStreamMemMap.h"
#include "OutputStreamMemMap.h"

#include "InputStreamBuff.h"
#include "OutputStreamBuff.h"
class MultiWayMerge
{
private : 
	OutputStreamMemMap* out;
	int nbInputStream;
	int nbEltOnInputStream;

	std::vector<InputStreamMemMap*> inputs;
	std::vector<int> inputsValues;
	std::vector<int> sortedOutputValues;

	void readInteger(InputStreamMemMap* inputstream, int position);
	void closeFile(InputStreamMemMap* inputstream);
	bool addNextValueToOutput();
	void writeOutput();

	void printStreamsValues();
	void printOutputValues();
public:
	MultiWayMerge(int nbInput, int nbElt, std::vector<InputStreamMemMap*> inputStreams);
	~MultiWayMerge();
	void start();
};

