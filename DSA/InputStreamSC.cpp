#include "stdafx.h"
#include "InputStreamSC.h"
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <exception>
#include <string>

InputStreamSC::InputStreamSC():end_of_file{false}{
	buf = malloc(sizeof(uint32_t));
}
#pragma warning(disable : 4996)
void InputStreamSC::open(const char * filename) {
	fileDescriptor = _open(filename, _O_BINARY | _O_RDONLY, _S_IREAD);
	if (fileDescriptor == -1) {
		throw std::exception(strerror(errno));
	}
}
uint32_t InputStreamSC::read_next() {
	int res = _read(fileDescriptor, buf, sizeof(uint32_t));
	if (res < 0) 
		throw std::exception("An error has occured when attempting to read");
	else if (res == 0) 
		end_of_file = true;
	return *((uint32_t*)buf);
}
bool InputStreamSC::end_of_stream() {
	return end_of_file;
}
InputStreamSC::~InputStreamSC() {
	free((uint32_t *)buf);
}