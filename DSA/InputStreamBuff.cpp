#include "stdafx.h"
#include "InputStreamBuff.h"
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <string>
#include <stdexcept>
#include <iostream>

InputStreamBuff::InputStreamBuff(unsigned bufferSize):end_of_file{false}{
    data_buffer.size = bufferSize;
    data_buffer.pos  = bufferSize;
    data_buffer.data = new uint32_t[bufferSize];
    buf              = (uint32_t*)malloc(bufferSize*sizeof(uint32_t));
}

InputStreamBuff::~InputStreamBuff() {
   // delete(buf);
}
#pragma warning(disable : 4996)
void InputStreamBuff::open(const char * filename) {
	fileDescriptor = _open(filename, _O_BINARY | _O_RDONLY, _S_IREAD);
	if (fileDescriptor == -1) {
		throw std::exception(strerror(errno));
	}
}

int InputStreamBuff::read_next() {
    int res;
    if (data_buffer.pos >= data_buffer.size-1){
        res = _read(fileDescriptor, buf, data_buffer.size * 4);
        if (res < 0)
            throw std::runtime_error("An error has occured when attempting to read");
        else if (res == 0)
            end_of_file = true;
        else
            for (int i = 0; i < data_buffer.size; ++i)
                data_buffer.data[i] = buf[i];

        data_buffer.pos = -1;
    }
    data_buffer.pos++;
    return data_buffer.data[data_buffer.pos];
}

bool InputStreamBuff::end_of_stream() {
    return end_of_file;
}

void InputStreamBuff::close()
{
	if (_close(fileDescriptor) == -1)
		throw std::runtime_error("Error while closing file");
}
