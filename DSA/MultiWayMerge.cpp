#include "stdafx.h"
#include "MultiWayMerge.h"
#include <iostream>

/*Implement a d-way merging algorithm that, given d sorted input streams of 32-bit integers,
creates a single output stream containing the elements from the input stream in sorted order.
The merging should use a priority queue (e.g., a heap) to obtain the next element to be output
at all times.*/
MultiWayMerge::MultiWayMerge(int nbInput, int nbElt, std::vector<InputStreamMemMap*> inputStreams) : nbInputStream(nbInput), nbEltOnInputStream(nbElt)
{
	if (nbInputStream <= 0)
	{
		throw std::invalid_argument("Number of input stream must be positif.");
	}
	inputs = inputStreams;
	for (size_t i = 0; i < nbInputStream; i++)
		inputsValues.push_back(0);
}

MultiWayMerge::~MultiWayMerge()
{
}

void MultiWayMerge::start() {
	bool allThreadFinish = false;
	out = new OutputStreamMemMap();
	out->create(L"output.txt", nbEltOnInputStream);
	bool eof = false;

	for (size_t i = 0; i < nbInputStream; i++)
		readInteger(inputs[i], i);

	while (!eof)
		eof = addNextValueToOutput();
	
	printOutputValues();
	
	for (size_t i = 0; i < nbInputStream; i++)
		closeFile(inputs[i]);

	writeOutput();
	out->close();
}

void MultiWayMerge::readInteger(InputStreamMemMap * inputstream, int position)
{
	if (!inputstream->end_of_stream())
		inputsValues[position] = inputstream->read_next();
}

void MultiWayMerge::closeFile(InputStreamMemMap * inputstream)
{
	inputstream->close();
	delete(inputstream);
}

/*
	Return true si tout les streams sont eof
*/
bool MultiWayMerge::addNextValueToOutput()
{
	int min = std::numeric_limits<int>::max();
	int choosedStream = -1;

	for (size_t i = 0; i < nbInputStream; i++)
	{
		if (!inputs[i]->end_of_stream() && inputsValues[i] < min)
		{
			min = inputsValues[i];
			choosedStream = i;
		}
	}
	if (choosedStream == -1)
		return true;

	sortedOutputValues.push_back(min);
	readInteger(inputs[choosedStream], choosedStream);
	return false;
}

void MultiWayMerge::writeOutput() {
	for (size_t i = 0; i < sortedOutputValues.size(); i++)
	{
		out->write(sortedOutputValues.back());
		sortedOutputValues.pop_back();
	}
}

void MultiWayMerge::printStreamsValues()
{
	for (size_t i = 0; i < nbInputStream; i++)
	{
		std::cout << inputsValues[i] << " ";
	}
}

void MultiWayMerge::printOutputValues()
{
	for (size_t i = 0; i < sortedOutputValues.size(); i++)
	{
		std::cout << sortedOutputValues[i] << " ";
	}
	std::cout << std::endl;
}