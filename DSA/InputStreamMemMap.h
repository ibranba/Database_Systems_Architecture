#pragma once

#include <tchar.h>
#include <stdio.h>
#include <atlstr.h>
#include <cstdint>
#include <string>

class InputStreamMemMap
{
private:
	bool mappingopen;
	const uint32_t * data;
	unsigned readCounter;
	unsigned nbRemElementsBuffer;
	unsigned mappingSize;
	LPCWSTR filename;
	bool eof;
	int offset;
	DWORD filesize;
	void openFile(LPCWSTR filename);
	void createMapping(DWORD FILE_MAP_START, DWORD BUFFSIZE);
	LPVOID lpMapAddress;  // pointer to the base address of the
						  // memory-mapped region
	HANDLE hMapFile;      // handle for the file's memory-mapped region
	HANDLE hFile;         // the file handle


public:
	InputStreamMemMap();
	~InputStreamMemMap();
	void open(LPCWSTR filename,int numberOfElements);
	void openWithOffset(LPCWSTR aFilename, int numberOfElements, int offset);

	uint32_t read_next();
	bool end_of_stream();
	void close();
};

