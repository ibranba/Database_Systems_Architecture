#pragma once
#include <cstdint>
class InputStreamSC {

private:
	int fileDescriptor;
	void * buf;
	bool end_of_file;

public:
	InputStreamSC();
	~InputStreamSC();
	void open(const char * filename);
	uint32_t read_next();
	bool end_of_stream();
};