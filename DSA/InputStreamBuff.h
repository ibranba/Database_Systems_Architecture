#pragma once
#include <cstdint>

class InputStreamBuff {
    typedef struct
    {
        unsigned size;
        int pos;
		uint32_t * data;
    } Data_buffer;

private:
    int fileDescriptor;
	uint32_t * buf;
    bool end_of_file;
    Data_buffer data_buffer;

    int fileSize;
    int filePos;

public:
	InputStreamBuff(unsigned bufferSize);
    ~InputStreamBuff();
    void open(const char * filename);
    int read_next();
    bool end_of_stream();
	void close();
};