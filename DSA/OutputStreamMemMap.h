#pragma once
#include <tchar.h>
#include <stdio.h>
#include <atlstr.h>
#include <cstdint>
#include <string>

typedef std::basic_string<TCHAR> TCHARstring;
class OutputStreamMemMap
{
private:
	bool mappingopen;
	uint32_t* data;
	LPCWSTR file;
	unsigned writeCounter;
	unsigned nbRemElementsBuffer;
	unsigned mappingSize;
	DWORD filesize;
	LPVOID lpMapAddress;  
	HANDLE hMapFile;
	HANDLE hFile;
	void createMapping(DWORD FILE_MAP_START, DWORD BUFFSIZE);
	void openFile(LPCWSTR filename);

public:
	OutputStreamMemMap();
	~OutputStreamMemMap();
	void create(LPCWSTR filename, int nbElements);
	void write(uint32_t n);
	void close();
};


