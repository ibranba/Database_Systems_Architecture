#include "stdafx.h"
#include "BenchmarkExternalMultiWayMerge.h"
#include <time.h> 
#include <vector>
#include <fstream>
#include "ExternalMultiWaySort.h"


std::fstream benchMemMapFile_M10_B10;
std::fstream benchMemMapFile_M10_B100;
std::fstream benchMemMapFile_M100_B10;
std::fstream benchMemMapFile_M100_B100;


void externalMultiWaySort(unsigned M, unsigned D, std::fstream & output) {
	OutputStreamMemMap out{};
	int nbElt = 26214400;
	std::string filename = "externalSortFile";
	srand(time(NULL));
	int randomValue;
	std::wstring stemp = std::wstring(filename.begin(), filename.end());
	LPCWSTR sw = stemp.c_str();
	out.create(sw, 26214400);
	for (uint32_t i = 0; i < nbElt; i++) {
		out.write(rand() % 100);
	}
	out.close();

	ExternalMultiWaySort externalMultiWaySort(M, D, filename, nbElt);

	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	externalMultiWaySort.start();

	QueryPerformanceCounter(&t2);

	output << (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart << "\n";
	std::remove(filename.c_str());

}
void benchmark() {
	benchMemMapFile_M10_B10.open("results/benchMemMapFile_M10_B10",std::fstream::out);
	benchMemMapFile_M10_B100.open("results/benchMemMapFile_M10_B100", std::fstream::out);
	benchMemMapFile_M100_B10.open("results/benchMemMapFile_M100_B10", std::fstream::out);
	benchMemMapFile_M100_B100.open("results/benchMemMapFile_M100_B100", std::fstream::out);

	externalMultiWaySort(262144, 2, benchMemMapFile_M10_B10);
	/*externalMultiWaySort(10, 100, benchMemMapFile_M10_B100);
	externalMultiWaySort(100, 10, benchMemMapFile_M100_B10);
	externalMultiWaySort(100, 100, benchMemMapFile_M100_B100);*/

	benchMemMapFile_M10_B10.close();
	benchMemMapFile_M10_B100.close();
	benchMemMapFile_M100_B10.close();
	benchMemMapFile_M100_B100.close();
}