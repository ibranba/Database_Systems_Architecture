#include "stdafx.h"
#include "OutputStreamMemMap.h"
#include "InputStreamMemMap.h"
#include "InputStream2.h"
#include "OutputStream2.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <windows.h>
#include "OutputStreamBuff.h"
#include "InputStreamBuff.h"
#include "InputStreamSC.h"
#include "OutputStreamSC.h"
#include "BenchmarkExternalMultiWayMerge.h"


std::fstream benchMemMapFile256;
std::fstream benchMemMapFile2621440;
std::fstream benchMemMapFile26214400;
std::fstream benchMemMapFile131072000;
std::fstream benchMemMapFile268435456;

std::fstream benchSysCallFile;
std::fstream benchCFunctFile;

std::fstream benchBufFile1024;
std::fstream benchBufFile10485760;
std::fstream benchBufFile104857600;
std::fstream benchBufFile1310720;

void benchExtMergeSort();
void benchSysCall() {
	const char * file = "benchmark_files/fileSysCall";
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);

	OutputStreamSC out{};

	out.create(file);
	//START TIMER
	QueryPerformanceCounter(&t1);
	for (uint32_t i = 0; i < 268435456; i++)
		out.write(i);
	out.close();

	InputStreamSC input{};
	input.open(file);
	while (!input.end_of_stream())
		input.read_next();

	QueryPerformanceCounter(&t2);

	benchSysCallFile << (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart << "\n";
	std::remove(file);
}
void benchCFunct() {
	const char * file = "benchmark_files/filebenchCfunct";
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);
	
	OutputStream2 out{};
	
	out.create(file);
	//START TIMER
	QueryPerformanceCounter(&t1);
	for (uint32_t i = 0; i < 268435456; i++)
		out.write(i);
	out.close();
	
	InputStream2 input{};
	input.open(file);
	while (!input.end_of_stream())
		input.read_next();

	QueryPerformanceCounter(&t2);

	benchCFunctFile << (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart << "\n";
	std::remove(file);
}
void benchBuf(unsigned buff, std::fstream & output) {
	const char * file = "benchmark_files/benchBuf";
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);

	OutputStreamBuff out{buff};

	out.create(file);
	//START TIMER
	QueryPerformanceCounter(&t1);
	for (uint32_t i = 0; i < 268435456; i++)
		out.write(i);
	out.close();

	InputStreamBuff input{ buff };
	input.open(file);
	while (!input.end_of_stream())
		input.read_next();

	QueryPerformanceCounter(&t2);

	output << (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart << "\n";
	std::remove(file);

}
void benchMemMap(unsigned buff,  std::fstream & output) {

	LPCWSTR file = L"benchmark_files/benchMemMap";
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);

	OutputStreamMemMap outputMemMap{};

	outputMemMap.create(file, buff);
	//START TIMER
	QueryPerformanceCounter(&t1);
	for (uint32_t i = 0; i < 268435456; i++)
		outputMemMap.write(i);
	outputMemMap.close();

	InputStreamMemMap inputMemMap{};
	inputMemMap.open(file,buff);
	while (!inputMemMap.end_of_stream())
		inputMemMap.read_next();

	inputMemMap.close();
	QueryPerformanceCounter(&t2);

	output << (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart << "\n";
	std::remove("benchmark_files / benchMemMap");
}


int main() {
	benchExtMergeSort();
}
void benchExtMergeSort() {
	benchmark();
}
void benchStreams() {
	CreateDirectory(L"results", NULL);
	CreateDirectory(L"benchmark_files", NULL);
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	std::cout << "Start" << "\n";

	benchMemMapFile256.open("results/benchMemMapFile1024.txt", std::fstream::out);
	benchMemMapFile2621440.open("results/benchMemMapFile10485760.txt", std::fstream::out);
	benchMemMapFile26214400.open("results/benchMemMapFile104857600.txt", std::fstream::out);
	benchMemMapFile131072000.open("results/benchMemMapFile524288000.txt", std::fstream::out);
	benchMemMapFile268435456.open("results/benchMemMapFile1073741824.txt", std::fstream::out);

	/*benchCFunctFile.open("results/benchCFunct.txt", std::fstream::out);
	benchCFunctFile << "#time in milliseconds" << "\n";

	benchBufFile1024.open("results/benchBuf1024.txt", std::fstream::out);
	benchBufFile10485760.open("results/benchBufFile10485760.txt", std::fstream::out);
	benchBufFile104857600.open("results/benchBufFile104857600.txt", std::fstream::out);
	benchBufFile1310720.open("results/benchBufFile1310720.txt", std::fstream::out);

	benchSysCallFile.open("results/benchSysCall.txt", std::fstream::out);
	benchSysCallFile << "#time in milliseconds" << "\n";*/


	/*std::cout << "Starting System Call benchmark\n";
	for (int i = 0; i < 1; i++)
	benchSysCall();

	benchSysCallFile.close();
	std::cout << "Done System Call benchmark \n";


	std::cout << "Starting C funct benchmark\n";

	for (int i = 0; i < 1; i++)
	benchCFunct();

	benchCFunctFile.close();
	std::cout << "Done C funct benchmark\n";


	std::cout << "Starting Buffered IO benchmark\n";*/

	//1024 bytes
	/*for (int i = 0; i < 5; i++)
	benchBuf(256, benchBufFile1024);

	//5 Mo
	for (int i = 0; i < 5; i++)
	benchBuf(1310720, benchBufFile1310720);

	/* //10 Mo*/
	/*for (int i = 0; i < 5; i++)
	benchBuf(2621440, benchBufFile10485760);

	//100 Mo
	for (int i = 0; i < 5; i++)
	benchBuf(26214400, benchBufFile104857600);

	std::cout << "Done Buff benchmark\n";


	*/
	/*
	std::cout << "Starting MemMap benchmark\n";


	for (int i = 0; i < 1; i++)
	benchMemMap(256,benchMemMapFile256);

	benchMemMapFile256.close();
	std::cout << "Done MemMap 1024b benchmark\n";

	*/

	for (int i = 0; i < 5; i++)
		benchMemMap(2621440, benchMemMapFile2621440);

	benchMemMapFile2621440.close();
	std::cout << "Done MemMap 10M benchmark\n";

	/*	for (int i = 0; i < 5; i++)
	benchMemMap(26214400, benchMemMapFile26214400);

	benchMemMapFile26214400.close();

	std::cout << "Done MemMap 100 Mo benchmark\n";

	for (int i = 0; i < 5; i++)
	benchMemMap(131072000, benchMemMapFile131072000);

	benchMemMapFile131072000.close();

	std::cout << "Done MemMap 500 Mo benchmark\n";



	for (int i = 0; i < 5; i++)
	benchMemMap(268435456, benchMemMapFile268435456);

	benchMemMapFile268435456.close();

	std::cout << "Done MemMap 1GB benchmark\n";*/

	std::cout << "END" << "\n";
	QueryPerformanceCounter(&t2);
	std::cout << "Execution time :" << (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart << std::endl;


	benchBufFile1024.close();
	benchBufFile1310720.close();
	benchBufFile10485760.close();
	benchBufFile104857600.close();

}