#pragma once
#include <cstdint>
class OutputStreamSC
{
private:

	int fileDescriptor;
	void * buf;
public:
	OutputStreamSC();
	~OutputStreamSC();
	void create(const char * filename);
	void write(uint32_t n);
	void close();
};

