#include "stdafx.h"
#include "OutputStreamBuff.h"
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <exception>
#include <string>
#include <stdexcept>
#include <iostream>
#include <atlstr.h>  



OutputStreamBuff::OutputStreamBuff(unsigned bufferSize)
{
	data_buffer.size = bufferSize;
	data_buffer.pos  = 0;
	data_buffer.data = new int[bufferSize];
	buf              = (char*)malloc(bufferSize);
}

OutputStreamBuff::~OutputStreamBuff()
{/*
	free(buf);
	delete data_buffer.data;
	buf = nullptr;*/
}
#pragma warning(disable : 4996)
void OutputStreamBuff::create(const char * filename)
{

	fileDescriptor = _open(filename, _O_BINARY | _O_WRONLY | _O_CREAT | _O_TRUNC, _S_IWRITE);
	if (fileDescriptor == -1) {
		throw std::exception(strerror(errno));
	}
}

void OutputStreamBuff::write(uint32_t n)
{
	data_buffer.data[data_buffer.pos] = n;
	data_buffer.pos++;
	if (data_buffer.pos >= data_buffer.size)
		emptyBuffer();
}

void OutputStreamBuff::emptyBuffer(){
	buf = data_buffer.data;
	_write(fileDescriptor, buf, sizeof(uint32_t) * data_buffer.size);
	data_buffer.pos = 0;
}

void OutputStreamBuff::close()
{
	emptyBuffer();
	if (_close(fileDescriptor) == -1)
		throw std::runtime_error("Error while closing file");
}
