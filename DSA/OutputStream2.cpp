#include "stdafx.h"
//
// Created by Issam on 22/11/2017.
//

#include "OutputStream2.h"
#include <stdlib.h>
#include <stdexcept>

using namespace std;

OutputStream2::OutputStream2(){
}

#pragma warning(disable : 4996)
void OutputStream2::create(string filename) {
    pFile = fopen(filename.c_str(), "wb");
    if (pFile == nullptr)
        throw std::runtime_error("Can't create your file");
}

void OutputStream2::write(uint32_t &value) {
    if (pFile != nullptr)
        fwrite(&value, sizeof(uint32_t), 1, pFile);
    else
        throw std::runtime_error("Can't write on file");
}

void OutputStream2::close() {
    if (pFile != nullptr)
        fclose(pFile);
}