#pragma once
#include "InputStreamMemMap.h"
#include "OutputStreamMemMap.h"
#include <vector>
class ExternalMultiWaySort
{
private : 
	OutputStreamMemMap * out;
	OutputStreamMemMap * outSorted;
	int nbInputStream;
	std::string filename;
	int fileSize;
	int nbMergeStep;
	int memorySize;

	std::vector<InputStreamMemMap*> inputs;
	std::vector<int> nbEltToRead;
	std::vector<std::vector<int>> heapMerge;
	std::vector<int> posOnMergedHeap;

	void openFile(InputStreamMemMap* inputstream, std::string filename, int position, int offset, bool read);
	void readInteger(InputStreamMemMap * inputstream, int position);
	int getMinValue(int begin, int end);
	void sortInputStreams();
	void printHeap();
	void writeOutput();
	void mergeBlocks();
public:
	ExternalMultiWaySort(int m, int d, std::string filename, int fileSize);
	~ExternalMultiWaySort();
	void start();
};

