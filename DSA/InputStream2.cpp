#include "stdafx.h"
#include "InputStream2.h"
using namespace std;

InputStream2::InputStream2(){
    filePos = 0;
}

#pragma warning(disable : 4996)
void InputStream2::open(string path) {
    pFile = fopen(path.c_str(), "rb");
    if (pFile == nullptr)
        throw std::runtime_error("Can't open the file");

    fseek(pFile, 0, SEEK_END);
    fileSize = ftell(pFile)/4;
    rewind(pFile);
}

uint32_t InputStream2::read_next() {
    int output;
    if (pFile != nullptr){
        filePos++;
		uint32_t result = fread(&output, sizeof(uint32_t), 1, pFile);
        if (result < 0)
            throw std::runtime_error("An error has occured when attempting to read");
        else if(result == 0)
            end_of_file = true;
    }else
        throw std::runtime_error("Can't read on file");

    return output;
}

bool InputStream2::end_of_stream() {
    return filePos == fileSize;
}

void InputStream2::close() {
	if (pFile != nullptr)
		fclose(pFile);
}

