#include "stdafx.h"
#include "OutputStreamSC.h"
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <exception>
#include <string>

OutputStreamSC::OutputStreamSC()
{
	buf = malloc(sizeof(uint32_t));
}

OutputStreamSC::~OutputStreamSC()
{
	free((int *)buf);
}
#pragma warning(disable : 4996)
void OutputStreamSC::create(const char * filename)
{

	fileDescriptor = _open(filename, _O_BINARY |_O_WRONLY |_O_CREAT | _O_TRUNC, _S_IWRITE);
	if (fileDescriptor == -1) {
		throw std::exception(strerror(errno));
	}
}

void OutputStreamSC::write(uint32_t n)
{
	*(uint32_t *)buf = n;
	_write(fileDescriptor,buf,sizeof(uint32_t));
}

void OutputStreamSC::close()
{
	if (_close(fileDescriptor) == -1) {
		throw std::exception("Error while closing file");
	}
	
}
